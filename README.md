# SingletonPattern

Singleton Pattern is one of the Creational design pattern. The singleton pattern puts restriction on object creation process; it restricts the instantiation of a class to one object. If we are using Singleton pattern, no need to instantiate class object, its the responsibility of the class to provide the way to access its one and only one object.

If your application deals with below questions then Singleton Pattern could be your solution.

* If a class need to have only one instance
* The class needs to be centrally accessible.
* You need to control how the class instances are created.
